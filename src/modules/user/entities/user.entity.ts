import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany, ManyToMany,
} from 'typeorm';
import {AddressEntity} from "../../address/entities/address.entity";
import {RoleEntity} from "../../role/entities/role.entity";
import {AvatarEntity} from "../../avatar/entities/avatar.entity";
import {GenderEnum} from '../enums/gender.enum';

@Entity()
export class UserEntity {

  @PrimaryGeneratedColumn({type: 'int'})
  id: number;

  @ManyToMany(type => RoleEntity, role => role.user)
  role: RoleEntity[]

  @OneToMany(type => AddressEntity, address => address.user)
  address: AddressEntity[]

  @OneToMany(type => AvatarEntity, avatar => avatar.user)
  avatar: AvatarEntity[]

  @Column({
    type: "varchar",
    default: null,
  })
  email: string;

  @Column({
    type: "varchar",
    default: null,
  })
  phone: string;

  @Column({
    type: "varchar",
    nullable: false,
    length: 100,
  })
  firstName: string;

  @Column({
    type: "varchar",
    default: null,
    length: 100,
  })
  lastName: string;

  @Column({
    type: "varchar",
    default: null,
    length: 100,
  })
  middleName: string;

  @Column({
    type: "varchar",
    default: null,
  })
  gender: GenderEnum;

  @Column({
    type: "varchar",
    default: null,
  })
  password: string;
}