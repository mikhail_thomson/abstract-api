import {Document} from "mongoose";
import {IAddress} from "../../address/interfaces/address.interface";
import {IRole} from "../../role/interfaces/role.interface";
import {IAvatar} from "../../avatar/interfaces/avatar.interface";

export interface IUser extends Document {
  readonly id: string;
  readonly email: string;
  readonly avatar: Array<IAvatar>;
  readonly firstName: string;
  readonly lastName: string;
  readonly gender: string;
  readonly address: Array<IAddress>;
  readonly phone: string;
  readonly role: Array<IRole>;
  readonly password: string;
}