import * as mongoose from 'mongoose'
import { GenderEnum } from "../enums/gender.enum";

export const UserSchema = new mongoose.Schema({
  // id: {
  //   type: String,
  //   required: true,
  // },
  // email: {
  //   type: String,
  //   default: null,
  // },
  avatarId: {
    type: [String],
    default: null,
  },
  // firstName: {
  //   type: String,
  //   default: null,
  // },
  // lastName: {
  //   type: String,
  //   default: null,
  // },
  gender: {
    type: String,
    required: true,
    enum: Object.values(GenderEnum)
  },
  // addressId: {
  //   type: [String],
  //   default: null,
  // },
  phoneId: {
    type: [String],
    default: null,
  },
  roleId: {
    type: [String],
    required: true,
  },
  password: {
    type: String,
    required: true,
  }
});

UserSchema.index(
  {
    id: 1,
  },
  {
    unique: true,
  })