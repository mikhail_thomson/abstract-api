import { Injectable } from '@nestjs/common';
import {Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";

import {UserEntity} from "./entities/user.entity";
import {IUser} from './interfaces/user.interface';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private userRepository: Repository<UserEntity>
  ) {}

  findAll(): Promise<UserEntity[]> {
    return this.userRepository.find()
  }

  findOne(id: number): Promise<UserEntity> {
    return this.userRepository.findOne(id)
  }

  create(user:IUser) {
    this.userRepository.create()

  }

  async remove(id: number): Promise<void> {
    await this.userRepository.delete(id)
  }
}
