import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';
import {IUser} from "../../user/interfaces/user.interface";
import {UserEntity} from "../../user/entities/user.entity";

@Entity()
export class AddressEntity {

  @PrimaryGeneratedColumn(
    {
      type: 'int'
    })
  id: number;

  @ManyToOne(type => UserEntity, user => user.address)
  user: IUser

  @Column({ default: null })
  country: string;

  @Column({default: null})
  city: string;

  @Column({ default: null })
  street: string;

  @Column({ default: null })
  build: string;

  @Column({ default: null })
  apartment: string;

}