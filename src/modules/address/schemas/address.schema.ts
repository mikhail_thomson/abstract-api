import * as mongoose from 'mongoose';

export const AddressSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
  },
  country: {
    type: String,
    default: null,
  },
  city: {
    type: String,
    default: null,
  },
  street: {
    type: String,
    default: null,
  },
  build: {
    type: String,
    default: null,
  },
  apartment: {
    type: String,
    default: null,
  }
})