import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';

import { AddressEntity } from './entities/address.entity';

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(AddressEntity)
    private addressRepository: Repository<AddressEntity>,
    private connection: Connection,
  ) {
  }

  findAll(): Promise<AddressEntity[]> {
    return this.addressRepository.find();
  }

  findOne(id: number): Promise<AddressEntity> {
    return this.addressRepository.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.addressRepository.delete(id);
  }
}
