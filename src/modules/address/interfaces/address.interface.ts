import {Document} from "mongoose";

export interface IAddress extends Document {
  id: number;
  country: string;
  city: string;
  street: string;
  build: string;
  apartment: string;
}