import {Document} from "mongoose";

export interface IAvatar extends Document {
  readonly id: string,
  readonly src: string,
}