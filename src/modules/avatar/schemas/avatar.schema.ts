import * as mongoose from 'mongoose';

export const AvatarSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
  },
  src: {
    type: String,
    default: null,
  },
})