import { Injectable } from '@nestjs/common';
import {Connection, Repository} from "typeorm";
import {InjectRepository} from "@nestjs/typeorm";

import {AvatarEntity} from "./entities/avatar.entity";
import {IUser} from '../user/interfaces/user.interface';

@Injectable()
export class AvatarService {
  constructor(
    @InjectRepository(AvatarEntity)
    private avatarRepository: Repository<AvatarEntity>,
    private connection: Connection
  ) {}

  findAll(): Promise<AvatarEntity[]> {
    return this.avatarRepository.find()
  }

  findOne(id: number): Promise<AvatarEntity> {
    return this.avatarRepository.findOne(id)
  }

  findByUserId(user: IUser): Promise<AvatarEntity> {
    return this.avatarRepository.findOne(user.id)
  }

  async remove(id: number): Promise<void> {
    await this.avatarRepository.delete(id)
  }
}
