import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from "typeorm";
import {UserEntity} from "../../user/entities/user.entity";

@Entity()
export class AvatarEntity {

  @PrimaryGeneratedColumn(
    {
      type: 'int'
    }
  )
  id: number;

  @ManyToOne(type => UserEntity, user => user.avatar)
  user: UserEntity

  @Column(
    {
      type: "text"
    }
  )
  src: string;
}