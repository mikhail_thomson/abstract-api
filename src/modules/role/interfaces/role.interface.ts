import {Document} from "mongoose";

export interface IRole extends Document {
  readonly id: string;
  readonly name: string;
}