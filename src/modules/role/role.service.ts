import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';

import { RoleEntity } from './entities/role.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(RoleEntity)
    private roleRepository: Repository<RoleEntity>,
    private connection: Connection,
  ) {
  }

  findAll(): Promise<RoleEntity[]> {
    return this.roleRepository.find();
  }

  findById(id: number): Promise<RoleEntity> {
    return this.roleRepository.findOne(id);
  }

  async remove(id: number): Promise<void> {
    await this.roleRepository.delete(id);
  }


}
