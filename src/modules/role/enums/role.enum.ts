export enum RoleEnum {
  guest = 'guest',
  user = 'user',
  admin = 'admin',
  root = 'root',
}