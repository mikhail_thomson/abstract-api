import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
} from "typeorm";
import { RoleEnum } from "../enums/role.enum";
import { UserEntity } from "../../user/entities/user.entity";

@Entity()
export class RoleEntity {

  @PrimaryGeneratedColumn(
    {
      type: 'int'
    })
  id: number;

  @ManyToMany(type => UserEntity, user => user.role)
  user: UserEntity;

  @Column(
    {
      type: 'varchar',
      default: 'guest',
    })
  type: RoleEnum;
}