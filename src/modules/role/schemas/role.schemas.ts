import * as mongoose from 'mongoose'
import {RoleEnum} from "../enums/role.enum";

export const RoleSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
    enum: Object.values(RoleEnum)
  }
})