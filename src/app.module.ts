// External Modules
import { Module } from '@nestjs/common';
import { ConfigModule } from "@nestjs/config";
// import { MongooseModule} from "@nestjs/mongoose";
import { TypeOrmModule } from '@nestjs/typeorm';

// Internal Modules
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import {AvatarModule} from "./modules/avatar/avatar.module";
import {AddressModule} from "./modules/address/address.module";
import {RoleModule} from "./modules/role/role.module";
import {AddressEntity} from "./modules/address/entities/address.entity";
import {RoleEntity} from "./modules/role/entities/role.entity";
import {AvatarEntity} from "./modules/avatar/entities/avatar.entity";
import {UserEntity} from "./modules/user/entities/user.entity";

@Module({
  imports: [
    UserModule,
    AuthModule,
    AvatarModule,
    AddressModule,
    RoleModule,
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    // MongooseModule.forRoot(process.env.MONGODB_CONNECTION_STRING, {
    //   useNewUrlParser: true,
    //   useUnifiedTopology: true,
    // }),
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: process.env.MYSQL_HOST ? process.env.MYSQL_HOST : 'localhost',
      port: process.env.MYSQL_PORT ? Number(process.env.MYSQL_PORT) : 3306,
      username: process.env.MYSQL_URER ? process.env.MYSQL_URER : 'asd',
      password: process.env.MYSQL_PASSWORD ? process.env.MYSQL_PASSWORD :'asd',
      database: process.env.MYSQL_DATABASE ? process.env.MYSQL_DATABASE : 'abstract',
      entities: [
        AddressEntity,
        RoleEntity,
        AvatarEntity,
        UserEntity,
      ],
      synchronize: true,
      retryAttempts: 10,
      retryDelay: 3000,
      autoLoadEntities: false,
      keepConnectionAlive: false,
      logging: [
        'query',
        'error',
        'schema',
        'warn',
        'info',
        'log',
      ],
      maxQueryExecutionTime: 1000,
      logger: 'file',
    }),
  ],
})
export class AppModule {}
